//--------------------------------- Constants ---------------------------------//
// "constant" functions which build sets of characters
// some day, these will work as const functions
//
// use std::iter::FromIterator;
// use std::collections::HashSet;
// const fn lower_alphas() -> HashSet<char> {
//     HashSet::from_iter(vec![
//         'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
//         'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
//     ])
// }

// fn upper_alphas() -> HashSet<char> {
// HashSet::from_iter(vec![
//     'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
//     'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
// ])
// }

// fn digits() -> HashSet<char> {
//     HashSet::from_iter(vec!['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'])
// }

// fn symbols_valid_in_identifiers() -> HashSet<char> {
//     HashSet::from_iter(vec!['!', '?', '-', '>', '<'])
// }

// Public statics exposing sets of characters for use elsewhere
// pub static LOWER_ALPHAS: HashSet<char> = lower_alphas();
pub static LOWER_ALPHAS: [char; 26] = [
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
    's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
];

// pub static UPPER_ALPHAS: HashSet<char> = upper_alphas();
pub static UPPER_ALPHAS: [char; 26] = [
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
    'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
];

// pub static DIGITS: HashSet<char> = digits();
pub static DIGITS: [char; 10] = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
// pub static ALL_ALPHAS: HashSet<char> = lower_alphas().into_iter().chain(upper_alphas()).collect();

pub static ALL_ALPHAS: [char; 52] = [
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
    's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
    'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
];

// pub static IDENTIFIER_CHARS: HashSet<char> =
// lower_alphas().into_iter().chain(upper_alphas()).chain(digits()).chain(symbols_valid_in_identifiers()).collect();
pub static IDENTIFIER_CHARS: [char; 67] = [
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
    's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
    'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1',
    '2', '3', '4', '5', '6', '7', '8', '9', '!', '?', '-', '<', '>',
];

// Characters which close or finish the reading of some symbol.
pub static TERMINATORS: [char; 2] = [' ', ')'];

// Single characters, nice to refer to by meaning or name
pub const QUOTE: char = '`';
pub const OPEN_PAREN: char = '(';
pub const CLOSE_PAREN: char = ')';
pub const STRING_QUOTE: char = '\'';
pub const COLON: char = ':';
pub const QUOTATION_MARK: char = '"';

// Constant strings we match against during parsing
pub const DEFINE: &str = "define";
pub const LAMBDA: &str = "lambda";