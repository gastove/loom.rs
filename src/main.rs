#![feature(slice_patterns)]
 
// Allow dead code (for now) to cut down on compiler chatter during development.
#![allow(dead_code)]
#![allow(unused_variables)]

#[macro_use] extern crate log;
extern crate simple_logger;
#[macro_use] extern crate text_io;

use reader::LoomProgram;

mod reader;
mod constants;

fn repl() {
    loop {
        print!("=> ");
        let s: String = read!("{}\n");
        println!("Got: {}", s);
    }
}

fn main() {
    simple_logger::init().unwrap();
    info!("Doing the thing");
    let s = String::from(":keyword");
    let mut program = LoomProgram::new();
    program.read(&s);
    repl();
}
