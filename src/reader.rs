use constants as lconst;
use std::collections::HashMap;
use std::iter::FromIterator; // `const` is a Rust keyword

fn is_symbol_char(ch: &char) -> bool {
    return lconst::IDENTIFIER_CHARS.contains(ch);
}

fn is_quote(ch: &char) -> bool {
    match ch {
        &lconst::QUOTE => true,
        _ => false,
    }
}

fn is_open_paren(ch: &char) -> bool {
    match ch {
        &lconst::OPEN_PAREN => true,
        _ => false,
    }
}

fn is_close_paren(ch: &char) -> bool {
    match ch {
        &lconst::CLOSE_PAREN => true,
        _ => false,
    }
}

fn is_colon(ch: &char) -> bool {
    match ch {
        &lconst::COLON => true,
        _ => false,
    }
}

fn is_terminator(ch: &char) -> bool {
    return lconst::TERMINATORS.contains(ch);
}

fn is_numeric(ch: &char) -> bool {
    lconst::DIGITS.contains(ch)
}

fn all_numeric(stack: &Vec<char>) -> bool {
    !stack.is_empty() && stack.iter().all(|c| c.is_numeric())
}

#[derive(Debug, PartialEq)]
enum LoomType {
    Keyword(String),
    String(String),
    Number(i64),
    Symbol(String),
    Unset,
}

#[derive(Debug, PartialEq)]
enum LoomForm {
    Atom(LoomType),
    Define(String),
    Implement,
    Lambda,
}

#[derive(Debug, PartialEq)]
struct LoomASTNode {
    data: LoomForm,
    leaves: Vec<LoomASTNode>,
}

impl LoomASTNode {
    pub fn new() -> LoomASTNode {
        LoomASTNode {
            data: LoomForm::Atom(LoomType::Unset),
            leaves: vec![],
        }
    }

    fn eval_atom(&self, atom: &LoomType) -> LoomType {
        match atom {
            LoomType::Number(i) => return LoomType::Number(*i),
            LoomType::Keyword(k) => return LoomType::Keyword(k.to_string()),
            LoomType::String(s) => return LoomType::String(s.to_string()),
            LoomType::Symbol(s) => return LoomType::Symbol(s.to_string()), // Soon: evaluate the symbol
            LoomType::Unset => return LoomType::Unset,                     // Eventually, error
        }
    }

    pub fn eval(&self, symbol_table: HashMap<String, LoomType>) -> LoomType {
        match &self.data {
            LoomForm::Atom(a) => self.eval_atom(a),
            _ => LoomType::Unset,
        }
    }
}

#[derive(Debug, PartialEq)]
struct ParseState {
    char_stack: Vec<char>,    // Characters coming in from the reader
    token_stack: Vec<String>, // Whole words we can't yet fully parse into AST nodes
    node_stack: Vec<LoomASTNode>,
}

impl ParseState {
    pub fn new() -> ParseState {
        ParseState {
            char_stack: Vec::new(),
            token_stack: Vec::new(),
            node_stack: Vec::new(),
        }
    }
}

// TODO: ParseState needs to be a field on self. Trying to keep up with passing
// it around is *horrible*
#[derive(Debug, PartialEq)]
pub struct LoomProgram {
    ast: LoomASTNode,
    symbol_table: HashMap<String, LoomType>,
    state: ParseState,
}

impl LoomProgram {
    pub fn new() -> LoomProgram {
        LoomProgram {
            ast: LoomASTNode::new(),
            symbol_table: HashMap::new(),
            state: ParseState::new(),
        }
    }

    fn insert_node_in_ast(&mut self, node: LoomASTNode) {
        println!("Inserting node {:?}\ninto AST {:?}", node, self.ast);
        // Try to get a node from the top of the stack.
        match self.state.node_stack.pop() {
            // We got a node; push the node passed as an argument to the stack
            // node as a leaf, then push the parent back on to the stack.
            Some(mut parent_node) => {
                parent_node.leaves.push(node);
                self.state.node_stack.push(parent_node);
                println!("After node insert to AST, here I am {:?}", self);
            }
            None => {
                println!("No node found, overwriting");
                self.ast = node
            }
        };
    }

    fn get_or_create_node(&mut self) -> LoomASTNode {
        let mut node = match self.state.node_stack.pop() {
            None => LoomASTNode::new(),
            Some(n) => n,
        };

        if node.data != LoomForm::Atom(LoomType::Unset) {
            self.state.node_stack.push(node);
            node = LoomASTNode::new();
        }

        node
    }

    fn parse_keyword(&mut self) {
        println!("Reducing a keyword...");
        let curr = self.state.char_stack.clone();
        let kwd = String::from_iter(curr);

        let mut node = match self.state.node_stack.pop() {
            None => LoomASTNode::new(),
            Some(n) => n,
        };

        node.data = LoomForm::Atom(LoomType::Keyword(kwd));

        self.insert_node_in_ast(node);

        self.state.char_stack.truncate(0);
    }

    fn parse_string(&mut self) {
        println!("Reducing a string");
        let curr = self.state.char_stack.clone();
        let loom_str = String::from_iter(curr);

        let mut node = self.get_or_create_node();

        node.data = LoomForm::Atom(LoomType::String(loom_str));

        self.insert_node_in_ast(node);

        self.state.char_stack = vec![];
        // return state;
    }

    fn parse_numeric(&mut self) {
        println!("Reducing a number");
        let curr = self.state.char_stack.clone();
        let str_rep = String::from_iter(curr);

        let loom_num = str_rep.parse::<i64>().unwrap();

        let mut node = self.get_or_create_node();

        node.data = LoomForm::Atom(LoomType::Number(loom_num));

        self.insert_node_in_ast(node);

        self.state.char_stack = vec![];
    }

    // (println "blep")                         -> General function call
    // (define foo <nearly anything>)           -> special term
    // (implement foo (lambda (x) (println x))) -> special term
    // (lambda (x) (println x))                 -> special term
    //
    // So, we're either hooking in to known special forms, or we're doing a
    // function call we'll need to resolve later, when we evaluate the AST.
    fn parse_define(&mut self) {
        println!("Parsing a define form from {:?}", self.state.token_stack);
        // The stack contains only "define", pass.
        if self.state.token_stack.len() == 1 {
            return;
        }

        match self.state.token_stack.pop() {
            Some(sym) => {
                println!("Got symbol: {}", sym);
                let form = LoomForm::Define(sym);

                let mut node = self.get_or_create_node();

                node.data = form;

                self.state.node_stack.push(node);

                self.state.token_stack.truncate(0);
            }
            None => (),
        }
    }

    fn parse_funcall(&mut self) {
        println!("Reducing a function call from {:?}", self.state.token_stack);
        match self.state.token_stack.pop() {
            Some(sym) => {
                let form = LoomForm::Atom(LoomType::Symbol(sym));

                let mut node = self.get_or_create_node();

                node.data = form;

                self.state.node_stack.push(node);

                self.state.token_stack.truncate(0);
            }
            None => (),
        }
    }

    fn parse_token(&mut self) {
        println!("Parsing a token...");

        let curr = self.state.char_stack.clone();

        let token: String = String::from_iter(curr);
        self.state.char_stack.truncate(0);

        self.state.token_stack.push(token);

        let tokens = self.state.token_stack.clone();

        // Try to match / handle specific known forms first; if we can't, try to
        // appropriately handle a given token.
        println!("Tokens is {:?}", tokens);
        match tokens[0].as_ref() {
            lconst::DEFINE => self.parse_define(),
            tok => self.parse_funcall(),
        };
    }

    fn reduce(&mut self, terminator: Option<char>) {
        println!("Reducing: {:?}", self.state);

        // Try to reduce a...
        match self.state.char_stack[..] {
            // Empty stack; noop
            [] => (),

            // ...Number
            [..] if all_numeric(&self.state.char_stack) => self.parse_numeric(),
            // ...String
            [lconst::QUOTATION_MARK, .., lconst::QUOTATION_MARK] => self.parse_string(),
            // ...String, but we're in progress -- push the terminator to the stack
            [lconst::QUOTATION_MARK, ..] => {
                // Handle the case that we're in an open string and need to push
                // one or more spaces to the stack.
                self.state.char_stack.push(terminator.unwrap());
            }
            // ...Keyword
            [lconst::COLON, ..] => self.parse_keyword(),

            // List literal (Soon™)
            // [lconst::QUOTE, lconst::OPEN_PAREN] => state,

            // Who knows? A symbol of some kind, probably
            _ => self.parse_token(),
        }
    }

    pub fn read(&mut self, code: &str) {
        info!("Reading...");

        for c in code.chars() {
            if is_open_paren(&c) {
                println!("Opening a new node and pushing to the stack");
                // An open paren means we're starting a new node
                let node = LoomASTNode::new();
                self.state.node_stack.push(node);
            } else if c.is_whitespace() {
                println!("Whitespace!");
                self.reduce(Some(c));
            } else if is_close_paren(&c) {
                println!("Found a close paren");
                self.reduce(None);

                match self.state.node_stack.pop() {
                    Some(n) => self.insert_node_in_ast(n),
                    None => println!(
                        "Something's fucked up, trying to close a node that doesn't exist."
                    ),
                }
            } else {
                println!("Pushing \"{:?}\" to the stack...", c);
                self.state.char_stack.push(c);
            }
        }

        // TODO: I'm not convinced anything gets returned at all... maybe a
        // Result<>? Just so you can tell it's working?
        self.reduce(None);
        println!("Final state: {:?}", self.state);
        ()
    }
}

#[cfg(test)]
mod reader_tests {

    use std::collections::HashMap;

    use reader;
    use reader::LoomASTNode;
    use reader::LoomForm;
    use reader::LoomProgram;
    use reader::LoomType;

    #[test]
    fn test_parse_primitive() {
        assert!(true);
    }

    #[test]
    fn test_parse_keyword() {
        let test_kwd = ":keyword";

        let mut program = LoomProgram::new();
        program.read(test_kwd);

        let expected_ast = LoomASTNode {
            data: LoomForm::Atom(LoomType::Keyword(String::from(test_kwd))),
            leaves: vec![],
        };

        let expected = LoomProgram {
            ast: expected_ast,
            symbol_table: HashMap::new(),
            state: reader::ParseState::new(),
        };

        assert_eq!(expected, program);
    }

    #[test]
    fn test_parse_string() {
        let test_str = "\"this is the test string, do you see it?\"";

        let mut program = LoomProgram::new();
        program.read(test_str);

        let expected_ast = LoomASTNode {
            data: LoomForm::Atom(LoomType::String(String::from(test_str))),
            leaves: vec![],
        };

        let expected = LoomProgram {
            ast: expected_ast,
            symbol_table: HashMap::new(),
            state: reader::ParseState::new(),
        };

        assert_eq!(expected, program);
    }

    #[test]
    fn test_all_numeric() {
        let should_true = vec!['1', '2', '3'];
        let should_false = vec!['1', 'k', '3'];
        let should_also_false = vec!['b', 'e', 'e', 'p'];

        assert_eq!(reader::all_numeric(&should_true), true);
        assert_eq!(reader::all_numeric(&should_false), false);
        assert_eq!(reader::all_numeric(&should_also_false), false);
    }

    #[test]
    fn test_parse_number() {
        // It comes in as a string, so we parse it.
        let test_number = "12345";
        let expected_number: i64 = 12345;

        let mut program = LoomProgram::new();
        program.read(test_number);

        let expected_ast = LoomASTNode {
            data: LoomForm::Atom(LoomType::Number(expected_number)),
            leaves: vec![],
        };

        let expected = LoomProgram {
            ast: expected_ast,
            symbol_table: HashMap::new(),
            state: reader::ParseState::new(),
        };

        assert_eq!(expected, program);
    }

    #[test]
    fn test_parse_token_define_string() {
        let test_definition = "(define beep \"boop\")";

        let expected_string = LoomType::String(String::from("\"boop\""));
        let expected_leaf = LoomASTNode {
            data: LoomForm::Atom(expected_string),
            leaves: vec![],
        };

        let expected_ast = LoomProgram {
            symbol_table: HashMap::new(),
            ast: LoomASTNode {
                data: LoomForm::Define(String::from("beep")),
                leaves: vec![expected_leaf],
            },
            state: reader::ParseState::new(),
        };

        let mut program = LoomProgram::new();
        program.read(test_definition);

        assert_eq!(expected_ast, program);
    }

    #[test]
    fn test_parse_token_define_number() {
        let test_definition = "(define beep 5)";

        let expected_number = LoomType::Number(5);
        let expected_leaf = LoomASTNode {
            data: LoomForm::Atom(expected_number),
            leaves: vec![],
        };

        let expected_ast = LoomProgram {
            symbol_table: HashMap::new(),
            ast: LoomASTNode {
                data: LoomForm::Define(String::from("beep")),
                leaves: vec![expected_leaf],
            },
            state: reader::ParseState::new(),
        };

        let mut program = LoomProgram::new();
        program.read(test_definition);

        assert_eq!(expected_ast, program);
    }

    #[test]
    fn test_parse_funcall() {
        let test_definition = "(println \"honk\")";

        let expected_string = LoomType::String(String::from("\"honk\""));
        let expected_leaf = LoomASTNode {
            data: LoomForm::Atom(expected_string),
            leaves: vec![],
        };

        let expected_ast = LoomProgram {
            symbol_table: HashMap::new(),
            ast: LoomASTNode {
                data: LoomForm::Atom(LoomType::Symbol(String::from("println"))),
                leaves: vec![expected_leaf],
            },
            state: reader::ParseState::new(),
        };

        let mut program = LoomProgram::new();
        program.read(test_definition);

        assert_eq!(expected_ast, program);
    }

}
